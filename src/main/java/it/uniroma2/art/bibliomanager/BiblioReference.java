/**
 * @author Massimo Pizzi
 * 
 * This class models a bibliographic reference
 */

package it.uniroma2.art.bibliomanager;

import java.util.ArrayList;

public class BiblioReference {
	
	private String citationKey,
					address, 
					annote, 
					author, 
					booktitle, 
					chapter, 
					edition, 
					editor, 
					howpublished, 
					institution, 
					journal, 
					key,
					month, 
					note, 
					number, 
					organization,
					pages,
					publisher, 
					school,
					series, 
					title, 
					type, 
					url, 
					volume, 
					year;
						
	private ArrayList<String> extra = new ArrayList<String>();
					


	public BiblioReference() {
		
	}
	
	
	public String getCitationKey() {
		return citationKey;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getAnnote() {
		return annote;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public String getBooktitle() {
		return booktitle;
	}

	public String getChapter() {
		return chapter;
	}

	public String getEdition() {
		return edition;
	}

	public String getEditor() {
		return editor;
	}

	public String getHowpublished() {
		return howpublished;
	}

	public String getInstitution() {
		return institution;
	}

	public String getJournal() {
		return journal;
	}

	public String getKey() {
		return key;
	}

	public String getMonth() {
		return month;
	}

	public String getNote() {
		return note;
	}

	public String getNumber() {
		return number;
	}

	public String getOrganization() {
		return organization;
	}

	public String getPages() {
		return pages;
	}

	public String getPublisher() {
		return publisher;
	}

	public String getSchool() {
		return school;
	}

	public String getSeries() {
		return series;
	}

	public String getTitle() {
		return title;
	}

	public String getType() {
		return type;
	}

	public String getUrl() {
		return url;
	}

	public String getVolume() {
		return volume;
	}

	public String getYear() {
		return year;
	}
	
	public String getExtra(int i){
		return extra.get(i);
	}
	
	public int extraSize() {
		return extra.size();
	}
	
	
	public void setCitationKey(String citk) {
		citationKey = citk;
	}
	
	public void setAddress(String add) {
		author = add;
	}
	
	public void setAnnote(String ann) {
		annote = ann;
	}
	
	public void setAuthor(String aut) {
		author = aut;
	}
	
	public void setBooktitle(String boo) {
		booktitle = boo;
	}
	
	public void setChapter(String cha) {
		chapter = cha;
	}
	
	public void setEdition(String edn) {
		edition = edn;
	}
	
	public void setEditor(String edr) {
		editor = edr;
	}
	
	public void setHowpublished(String hop) {
		howpublished = hop;
	}
	
	public void setInstitution(String ins) {
		institution = ins;
	}
	
	public void setJournal(String jou) {
		journal = jou;
	}
	
	public void setKey(String k) {
		key = k;
	}
	
	public void setMonth(String mon) {
		month = mon;
	}
	
	public void setNote(String not) {
		note = not;
	}
	
	public void setNumber(String num) {
		number = num;
	}
	
	public void setOrganization(String org) {
		organization = org;
	}
	
	public void setPages(String pag) {
		pages = pag;
	}
	
	public void setPublisher(String pub) {
		publisher = pub;
	}
	
	public void setSchool(String sch) {
		school = sch;
	}
	
	public void setSeries(String ser) {
		series = ser;
	}
	
	public void setTitle(String tit) {
		title = tit;
	}
	
	public void setType(String typ) {
		type = typ;
	}
	
	public void setUrl(String u) {
		url = u;
	}
	
	public void setVolume(String vol) {
		volume = vol;
	}
	
	public void setYear(String ye) {
		year = ye;
	}
	
	public void feedExtra(String xtr){
		extra.add(xtr);
	}
	
	/**
	 * Prints in console the non-empty fields of a reference.
	 * BibTex format is adopted as print format.
	 */
	public void printRef(){
		System.out.println("@"+type+"{"+citationKey+"},");
		if (address != null) System.out.println("\taddress={"+address+"},");
		if (annote != null) System.out.println("\tannote={"+annote+"},");
		if (author != null) System.out.println("\tauthor={"+author+"},");
		if (booktitle != null) System.out.println("\tbooktitle={"+booktitle+"},");
		if (chapter != null) System.out.println("\tchapter={"+chapter+"},");
		if (edition != null) System.out.println("\tedition={"+edition+"},");
		if (editor != null) System.out.println("\teditor={"+editor+"},");
		if (howpublished != null) System.out.println("\thowpublished={"+howpublished+"},");
		if (institution != null) System.out.println("\tinstitution={"+institution+"},");
		if (journal != null) System.out.println("\tjournal={"+journal+"},");
		if (key != null) System.out.println("\tkey={"+key+"},");
		if (month != null) System.out.println("\tmonth={"+month+"},");
		if (note != null) System.out.println("\tnote={"+note+"},");
		if (number != null) System.out.println("\tnumber={"+number+"},");
		if (organization != null) System.out.println("\torganization={"+organization+"},");
		if (pages != null) System.out.println("\tpages={"+pages+"},");
		if (publisher != null) System.out.println("\tpublisher={"+publisher+"},");
		if (school != null) System.out.println("\tschool={"+school+"},");
		if (series != null) System.out.println("\tseriess={"+series+"},");
		if (title != null) System.out.println("\ttitle={"+title+"},");
		if (type != null) System.out.println("\ttype={"+type+"},");
		if (url != null) System.out.println("\turl={"+url+"},");
		if (volume != null) System.out.println("\tvolume={"+volume+"},");
		if (year != null) System.out.println("\tyear={"+year+"},");
		if (extra != null) System.out.println("\textra={"+extra+"},");
		System.out.println("}");
	}
}