/**
 * @author Massimo Pizzi
 * 
 * This class allows to parse a BibTex file into a Reference object
 * @see BiblioReference
 */
package it.uniroma2.art.bibliomanager.formats;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import it.uniroma2.art.bibliomanager.BiblioReference;

public class BibParser{

	private FileOutputStream fileCreato;
	private PrintStream ps;
	private BufferedReader br;
	private FileReader fr;
	private String nomeFile;
	private final String[] parametri = new String[] {"citationKey",
			"address", 
			"annote", 
			"author", 
			"booktitle", 
			"chapter", 
			"edition", 
			"editor", 
			"howpublished", 
			"institution", 
			"journal", 
			"key",
			"month", 
			"note", 
			"number", 
			"organization",
			"pages",
			"publisher", 
			"school",
			"series", 
			"title", 
			"type", 
			"url", 
			"volume", 
			"year"};
	
	/*
	*private final String[] tipi = new String[] {"article",
	*		"book",
	*		"booklet",
	*		"conference",
	*		"inbook",
	*		"incollection",
	*		"inproceedings",
	*		"manual",
	*		"masterthesis",
	*		"misc",
	*		"phdthesis",
	*		"proceedings",
	*		"techreport",
	*		"unpublished"};
	*/	

	public BibParser(String s) {
		nomeFile = s;
	}

	
	/**
	 * Checks that in the line there is one of the fields admitted in the BibTex standard
	 * 
	 * @param s		the name of the parameter
	 * @return		
	 */
	public boolean canAcceptAsParam(String s) {

		boolean propRiconosciuta = false;
		
		for(int i = 0; i < parametri.length;i++) {	
			if((s.toLowerCase()).indexOf(parametri[i]) != -1) {
				propRiconosciuta = true;
			}
		}
		return propRiconosciuta;
	}
	
	/**
	 * Extracts the crucial information from the line
	 * 
	 * @param rigaCompleta		the complete line
	 * @return		only the information
	 */
	public String extractField(String rigaCompleta) {
		
		String info = rigaCompleta.substring(rigaCompleta.indexOf("{")+1, rigaCompleta.lastIndexOf("}"));
			
		return info;
	}
	
	/**
	 * Saves an information in the correct field of the Reference object
	 * 
	 * @param fonte		the reference
	 * @param rigaCompleta			the complete line
	 */
	public void setField(BiblioReference fonte, String rigaCompleta) {
		
		if(canAcceptAsParam(rigaCompleta)) {
			
			String field = extractField(rigaCompleta);
			String s = rigaCompleta.toLowerCase();
			
			if(s.indexOf("address") != -1) {
				fonte.setAddress(field);
			} else if(s.indexOf("annote") != -1) {
				fonte.setAnnote(field);
			} else if(s.indexOf("author") != -1) {
				fonte.setAuthor(field);
			} else if(s.indexOf("booktitle") != -1) {
				fonte.setBooktitle(field);
			} else if(s.indexOf("chapter") != -1) {
				fonte.setChapter(field);
			} else if(s.indexOf("edition") != -1) {
				fonte.setEdition(field);
			} else if(s.indexOf("editor") != -1) {
				fonte.setEditor(field);
			} else if(s.indexOf("howpublished") != -1) {
				fonte.setHowpublished(field);
			}  else if(s.indexOf("institution") != -1) {
				fonte.setInstitution(field);
			} else if(s.indexOf("journal") != -1) {
				fonte.setJournal(field);
			} else if(s.indexOf("key") != -1) {
				fonte.setKey(field);
			} else if(s.indexOf("month") != -1) {
				fonte.setMonth(field);
			} else if(s.indexOf("note") != -1) {
				fonte.setNote(field);
			} else if(s.indexOf("number") != -1) {
				fonte.setNumber(field);
			} else if(s.indexOf("organization") != -1) {
				fonte.setOrganization(field);
			} else if(s.indexOf("pages") != -1) {
				fonte.setPages(field);
			} else if(s.indexOf("publisher") != -1) {
				fonte.setPublisher(field);
			} else if(s.indexOf("school") != -1) {
				fonte.setSchool(field);
			} else if(s.indexOf("series") != -1) {
				fonte.setSeries(field);
			} else if(s.indexOf("title") != -1) {
				fonte.setTitle(field);
			} else if(s.indexOf("type") != -1) {
				fonte.setType(field);
			} else if(s.indexOf("url") != -1) {
				fonte.setUrl(field);
			} else if(s.indexOf("volume") != -1) {
				fonte.setVolume(field);
			} else if(s.indexOf("year") != -1) {
				fonte.setYear(field);
			}
		} else{
			fonte.feedExtra(rigaCompleta);
		}
	}

	/**
	 * Acquires a BibTex file and maps it into an ArrayList of Reference objects
	 * 
	 * @return		a list of all Reference objects
	 */
	public ArrayList<BiblioReference> importFile() throws IOException {		

		try {
			fr = new FileReader(nomeFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		br = new BufferedReader(fr);
		
		String rigaLetta;
		BiblioReference fonteCorrente = new BiblioReference();
		ArrayList<BiblioReference> bibliografia = new ArrayList<BiblioReference>();
		
		//while the doc is not finished yet
		while((rigaLetta=br.readLine()) != null) {
			
			if(rigaLetta.indexOf("@") != -1){
				
				fonteCorrente = new BiblioReference();
				
				fonteCorrente.setType(rigaLetta.substring(rigaLetta.indexOf("@")+1, rigaLetta.indexOf("{")));
				fonteCorrente.setCitationKey(rigaLetta.substring(rigaLetta.indexOf("{")+1, rigaLetta.indexOf(",")));
				
				rigaLetta = br.readLine();
				boolean stessaRef = true;

				while(rigaLetta!=null && stessaRef) {
					
					if(rigaLetta.indexOf("=") != -1){
						this.setField(fonteCorrente, rigaLetta);
					}else{
						stessaRef = false;
					}
					rigaLetta=br.readLine();
				}		
			}
			bibliografia.add(fonteCorrente);
		}
		return bibliografia;
	}
	
	/**
	 * Creates a BibTec file basing on the list of References in the param
	 * 
	 * @param bibliografia		a list of all the Reference objects to export in the file
	 */
	public void exportFile(ArrayList<BiblioReference> bibliografia) {		

	    try {
			fileCreato = new FileOutputStream(nomeFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    ps = new PrintStream(fileCreato);

	    for(int i = 0; i < bibliografia.size(); i++) {

	    	BiblioReference fonte = bibliografia.get(i);
			ps.println("@"+fonte.getType()+"{"+fonte.getCitationKey()+",");
			if (fonte.getAddress() != null) ps.println("\taddress={"+fonte.getAddress()+"},");
			if (fonte.getAnnote() != null) ps.println("\tannote={"+fonte.getAnnote()+"},");
			if (fonte.getAuthor() != null) ps.println("\tauthor={"+fonte.getAuthor()+"},");
			if (fonte.getBooktitle() != null) ps.println("\tbooktitle={"+fonte.getBooktitle()+"},");
			if (fonte.getChapter() != null) ps.println("\tchapter={"+fonte.getChapter()+"},");
			if (fonte.getEdition() != null) ps.println("\tedition={"+fonte.getEdition()+"},");
			if (fonte.getEditor() != null) ps.println("\teditor={"+fonte.getEditor()+"},");
			if (fonte.getHowpublished() != null) ps.println("\thowpublished={"+fonte.getHowpublished()+"},");
			if (fonte.getInstitution() != null) ps.println("\tinstitution={"+fonte.getInstitution()+"},");
			if (fonte.getJournal() != null) ps.println("\tjournal={"+fonte.getJournal()+"},");
			if (fonte.getKey() != null) ps.println("\tkey={"+fonte.getKey()+"},");
			if (fonte.getMonth() != null) ps.println("\tmonth={"+fonte.getMonth()+"},");
			if (fonte.getNote() != null) ps.println("\tnote={"+fonte.getNote()+"},");
			if (fonte.getNumber() != null) ps.println("\tnumber={"+fonte.getNumber()+"},");
			if (fonte.getOrganization() != null) ps.println("\torganization={"+fonte.getOrganization()+"},");
			if (fonte.getPages() != null) ps.println("\tpages={"+fonte.getPages()+"},");
			if (fonte.getPublisher() != null) ps.println("\tpublisher={"+fonte.getPublisher()+"},");
			if (fonte.getSchool() != null) ps.println("\tschool={"+fonte.getSchool()+"},");
			if (fonte.getSeries() != null) ps.println("\tseriess={"+fonte.getSeries()+"},");
			if (fonte.getTitle() != null) ps.println("\ttitle={"+fonte.getTitle()+"},");
			if (fonte.getType() != null) ps.println("\ttype={"+fonte.getType()+"},");
			if (fonte.getUrl() != null) ps.println("\turl={"+fonte.getUrl()+"},");
			if (fonte.getVolume() != null) ps.println("\tvolume={"+fonte.getVolume()+"},");
			if (fonte.getYear() != null) ps.println("\tyear={"+fonte.getYear()+"},");
			if (fonte.extraSize() != 0) {
				for(int agig = 0; agig < fonte.extraSize(); agig++){
					ps.println("\textra={"+fonte.getExtra(agig)+"},");
				}
			}
			ps.println("}");
	    }
	    ps.close();
	}
}