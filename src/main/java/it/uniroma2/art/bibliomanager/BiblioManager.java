/**
 * @author Massimo Pizzi
 * 
 * This class contains the main 4 functions of B-Man, from/to file to/from Reference and from/to Reference to/from ontology 
 */

package it.uniroma2.art.bibliomanager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import it.uniroma2.art.bibliomanager.formats.BibParser;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.query.Update;
import it.uniroma2.art.owlart.vocabulary.XmlSchema;

public class BiblioManager {

	private String filePath;
	private ArrayList<BiblioReference> bibliography;
	private BiblioReference currentReference;
	
	public BiblioManager() {

	}
	
	/**
	 * This class imports a file in a Reference object
	 * 
	 * @param fP	is the path of the file
	 * @see BiblioReference
	 * @throws IOException
	 */
	public void fiToBi(String fP) throws IOException{
		
		filePath = fP;
				
		if(filePath.endsWith(".bib")){
			BibParser parser = new BibParser(filePath);
			bibliography = parser.importFile();
		} else{
			//TODO implement here other file formats developed
		}	
	}
	
	/**
	 * This class exports a Reference object into a File
	 * 
	 * @param fP	is the desired path where the file must be created
	 * @param fF	is the format of the standard the file is desired to be converted in
	 * @see BiblioReference
	 * @throws IOException
	 */
	public void biToFi(String fP) {		
		filePath = fP;
		
		if(filePath.endsWith(".bib")){
			BibParser parser = new BibParser(filePath);
			parser.exportFile(bibliography);
		} else{
			//TODO implement here other file formats developed
		}
	}
	
	/**
	 * This class exports a Reference object into the Ontology
	 * @see BiblioReference
	 * @throws IOException
	 */
	@SuppressWarnings("deprecation")
	public void biToOn(OWLModel currentModel) throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException {
		
		for(int atur=0; atur<bibliography.size(); atur++){
			
			currentReference = bibliography.get(atur);
			
			String refId;
			if(currentReference.getCitationKey() != null) {
				refId = currentReference.getCitationKey();
			} else {
				refId = new String(Calendar.getInstance().getTime().toGMTString());
			}
			
			 String biblioReferenceSubClass = currentReference.getType();
			 biblioReferenceSubClass = biblioReferenceSubClass.substring(0, 1).toUpperCase() + biblioReferenceSubClass.substring(1).toLowerCase();
			 			
			//splits authors if there are many in the same reference
			String[] authors = currentReference.getAuthor().split(" and ");
			
			String query = "PREFIX : <http://art.uniroma2.it/bibliomanager#>" 
					+ "\nINSERT DATA{"
					+ "\n:" + refId + " a :"+biblioReferenceSubClass+".";

			if (currentReference.getAddress() != null) query += ("\n:" + refId + " :address \"" + currentReference.getAddress() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getAnnote() != null) query += ("\n:" + refId + " :annote \"" + currentReference.getAnnote() + "\"^^<"+XmlSchema.STRING+">.");
			
			for (int tlor = 0; tlor<authors.length; tlor++) {
				if (currentReference.getAuthor() != null) query += ("\n:" + refId + " :author \"" + authors[tlor] + "\"^^<"+XmlSchema.STRING+">.");
			}
			
			if (currentReference.getBooktitle() != null) query += ("\n:" + refId + " :booktitle \"" + currentReference.getBooktitle() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getChapter() != null) query += ("\n:" + refId + " :chapter \"" + currentReference.getChapter() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getEdition() != null) query += ("\n:" + refId + " :edition \"" + currentReference.getEdition() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getEditor() != null) query += ("\n:" + refId + " :editor \"" + currentReference.getEditor() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getHowpublished() != null) query += ("\n:" + refId + " :howpublished :" + currentReference.getHowpublished() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getInstitution() != null) query += ("\n:" + refId + " :institution \"" + currentReference.getInstitution() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getJournal() != null) query += ("\n:" + refId + " :journal \"" + currentReference.getJournal() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getKey() != null) query += ("\n:" + refId + " :key \"" + currentReference.getKey() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getMonth() != null) query += ("\n:" + refId + " :month \"" + currentReference.getMonth() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getNote() != null) query += ("\n:" + refId + " :note \"" + currentReference.getNote() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getNumber() != null) query += ("\n:" + refId + " :number \"" + currentReference.getNumber() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getOrganization() != null) query += ("\n:" + refId + " :organization \"" + currentReference.getOrganization() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getPages() != null) query += ("\n:" + refId + " :pages \"" + currentReference.getPages() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getPublisher() != null) query += ("\n:" + refId + " :publisher \"" + currentReference.getPublisher() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getSchool() != null) query += ("\n:" + refId + " :school \"" + currentReference.getSchool() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getSeries() != null) query += ("\n:" + refId + " :series \"" + currentReference.getSeries() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getTitle() != null) query += ("\n:" + refId + " :title \"" + currentReference.getTitle() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getType() != null) query += ("\n:" + refId + " :type \"" + currentReference.getType() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getUrl() != null) query += ("\n:" + refId + " :url \"" + currentReference.getUrl() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getVolume() != null) query += ("\n:" + refId + " :volume \"" + currentReference.getVolume() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.getYear() != null) query += ("\n:" + refId + " :year \"" + currentReference.getYear() + "\"^^<"+XmlSchema.STRING+">.");
			if (currentReference.extraSize() != 0) {
				for(int mfio = 0; mfio < currentReference.extraSize(); mfio++){
					query += ("\n:" + refId + " :extra \"" + currentReference.getExtra(mfio) + "\"^^<"+XmlSchema.STRING+">.");
				}
			}
			
			query += ("\n}");
			
			Update update = currentModel.createUpdateQuery(query);
			update.evaluate(false);
		}
	}
	
	/**
	 * This class builds a Reference object from the Ontology
	 * 
	 * @see BiblioReference
	 * @throws IOException
	 * 
	 * @param currentModel
	 */
	public void onToBi(OWLModel currentModel) throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException{

		bibliography = new ArrayList<>();
		String query = "PREFIX : <http://art.uniroma2.it/bibliomanager#>\n"
						+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
						+ "SELECT ?item"
						+ " ?address ?annote ?author ?booktitle ?chapter ?edition"
						+ " ?editor ?howpublished ?institution ?journal ?key ?month"
						+ " ?note ?number ?organization ?pages ?publisher ?school"
						+ " ?series ?title ?type ?url ?volume ?year\n"
						+ " WHERE {\n"
						+ "  ?sub rdfs:subClassOf :BiblioReference.\n"
						+ "	 ?item a ?sub.\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :address ?address.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :annote ?annote.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :author ?author.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :booktitle ?booktitle.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :chapter ?chapter.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :edition ?edition.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :editor ?editor.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :howpublished ?howpublished.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :institution ?institution.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :journal ?journal.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :key ?key.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :month ?month.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :note ?note.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :number ?number.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :organization ?organization.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :pages ?pages.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :publisher ?publisher.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :school ?school.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :series ?series.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :title ?title.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :type ?type.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :url ?url.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :volume ?volume.\n"
						+ "  }\n"
						+ "  OPTIONAL {\n"
						+ "   ?item :year ?year.\n"
						+ "  }\n"
						+ " }\n"
						+ "ORDER BY ?item";
		
		TupleQuery tuple = currentModel.createTupleQuery(query);
		
		
		TupleBindingsIterator tupleBindingsIterator = tuple.evaluate(false);
		TupleBindingsIterator futupleBindingsIterator = tuple.evaluate(false);
				
		TupleBindings futupleBindings = futupleBindingsIterator.getNext();
		TupleBindings tupleBindings;
		
		boolean nextReferenceIsDifferent = true;
		String cR = new String();
		String fcR = new String();
				
		while(tupleBindingsIterator.streamOpen()) {
			
			if(nextReferenceIsDifferent) {
				bibliography.add(new BiblioReference());
				currentReference = bibliography.get(bibliography.size()-1);
			}
					
			tupleBindings = tupleBindingsIterator.getNext();
			cR = tupleBindings.getBinding("item").getBoundValue().getNominalValue();
						
			if(futupleBindingsIterator.hasNext()){
				futupleBindings = futupleBindingsIterator.getNext();
				fcR = futupleBindings.getBinding("item").getBoundValue().getNominalValue();

				if(cR.equals(fcR)){
					nextReferenceIsDifferent = false;
				} else {
					nextReferenceIsDifferent = true;
				}
			}else{
				nextReferenceIsDifferent = true;
			}
			
			if(nextReferenceIsDifferent) {
			
				currentReference.setCitationKey(cR.substring(cR.indexOf("#")+1));
				if (tupleBindings.hasBinding("address")) {
					currentReference.setAddress(tupleBindings.getBinding("address").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("annote")) {
					currentReference.setAnnote(tupleBindings.getBinding("annote").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("author")) {
					if(currentReference.getAuthor() == null){
						currentReference.setAuthor(tupleBindings.getBinding("author").getBoundValue().getNominalValue());
					} else {
						currentReference.setAuthor(currentReference.getAuthor() + tupleBindings.getBinding("author").getBoundValue().getNominalValue());
					}
				}
				if (tupleBindings.hasBinding("booktitle")) {
					currentReference.setBooktitle(tupleBindings.getBinding("booktitle").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("chapter")) {
					currentReference.setChapter(tupleBindings.getBinding("chapter").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("edition")) {
					currentReference.setEdition(tupleBindings.getBinding("edition").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("editor")) {
					currentReference.setEditor(tupleBindings.getBinding("editor").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("howpublished")) {
					currentReference.setHowpublished(tupleBindings.getBinding("howpublished").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("institution")) {
					currentReference.setInstitution(tupleBindings.getBinding("institution").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("journal")) {
					currentReference.setJournal(tupleBindings.getBinding("journal").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("key")) {
					currentReference.setKey(tupleBindings.getBinding("key").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("month")) {
					currentReference.setMonth(tupleBindings.getBinding("month").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("note")) {
					currentReference.setNote(tupleBindings.getBinding("note").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("number")) {
					currentReference.setNumber(tupleBindings.getBinding("number").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("organization")) {
					currentReference.setOrganization(tupleBindings.getBinding("organization").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("pages")) {
					currentReference.setPages(tupleBindings.getBinding("pages").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("publisher")) {
					currentReference.setPublisher(tupleBindings.getBinding("publisher").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("school")) {
					currentReference.setSchool(tupleBindings.getBinding("school").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("series")) {
					currentReference.setSeries(tupleBindings.getBinding("series").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("title")) {
					currentReference.setTitle(tupleBindings.getBinding("title").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("type")) {
					currentReference.setType(tupleBindings.getBinding("type").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("url")) {
					currentReference.setUrl(tupleBindings.getBinding("url").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("volume")) {
					currentReference.setVolume(tupleBindings.getBinding("volume").getBoundValue().getNominalValue());
				}
				if (tupleBindings.hasBinding("year")) {
					currentReference.setYear(tupleBindings.getBinding("year").getBoundValue().getNominalValue());
				}
			} else {
				if (tupleBindings.hasBinding("author")) {
					if(currentReference.getAuthor() == null){
						currentReference.setAuthor(tupleBindings.getBinding("author").getBoundValue().getNominalValue() + " and ");
					} else {
						currentReference.setAuthor(currentReference.getAuthor() + tupleBindings.getBinding("author").getBoundValue().getNominalValue() + " and ");
					}
				}
			}
		}
		tupleBindingsIterator.close();
		futupleBindingsIterator.close();
	}	
}