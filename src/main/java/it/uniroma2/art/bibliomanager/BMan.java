/**
 * @author Massimo Pizzi
 * 
 * Using the Spring controllers, this class waits for a client request
 */

package it.uniroma2.art.bibliomanager;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.w3c.dom.Element;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.semanticturkey.generation.annotation.GenerateSTServiceController;
import it.uniroma2.art.semanticturkey.services.STServiceAdapter;
import it.uniroma2.art.semanticturkey.servlet.Response;
import it.uniroma2.art.semanticturkey.servlet.ServiceVocabulary.RepliesStatus;
import it.uniroma2.art.semanticturkey.servlet.XMLResponseREPLY;

import it.uniroma2.art.bibliomanager.BiblioManager;

@GenerateSTServiceController // Makes st-codegen-processor generate a Spring controller for this service
@Validated // Enables validation (useful when method parameters are annotated with constraints)
@Component // Defines a singleton bean for this class (avoiding XML configuration)
public class BMan extends STServiceAdapter {

	static int request;
	static String filePath;
	static String outputFormat;
	public ArrayList<BiblioReference> bibliografia;

	static BiblioManager biblioManager = new BiblioManager();

	/*
	 * this constructor can be deleted, it's just for demo purpose and it prints a message on karaf console
	 * when the Controller is instantiated
	 */
	public BMan() {
	}
	
	/**
	 * Answers to an importation request from the client
	 * 
	 * @param imPath
	 * 
	 * @return respXml
	 * 
	 * @throws ModelAccessException
	 * @throws IOException
	 * @throws UnsupportedQueryLanguageException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 */
	@GenerateSTServiceController // Makes st-codegen-processor publish this method in the generated controller
	public Response importBibliography(String imPath) throws ModelAccessException, IOException, UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException {
		// Creates a reply response (status is equal to RepliesStatus.ok)
		// The service adapter (extended by this service class) provides method to create other types of
		// response
		XMLResponseREPLY respXml = createReplyResponse(RepliesStatus.ok);

		// Gets the data element of the response
		Element dataElem = respXml.getDataElement();

		// Simply sets the text content of the data element
		dataElem.setTextContent("Hi " + imPath);
		
		OWLModel model = getOWLModel();
		
		biblioImp(model, imPath);

		// Returns the response object
		return respXml;

		// A service method may have response type equal to void. In that case, the generated controller
		// is in charge of creating an empty reply response with status OK.

		// An exception thrown by a service method propagates through the generated controller, and then a
		// Controller Advice defined in the servlet context wraps it in the proper XML response.
	}
	
	/**
	 * Answers to an exportation request from the client
	 * 
	 * @param exPath
	 * 
	 * @return respXml
	 * 
	 * @throws ModelAccessException
	 * @throws IOException
	 * @throws UnsupportedQueryLanguageException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 */
	@GenerateSTServiceController // Makes st-codegen-processor publish this method in the generated controller
	public Response exportBibliography(String exPath) throws ModelAccessException, IOException, UnsupportedQueryLanguageException, MalformedQueryException, QueryEvaluationException {

		// Creates a reply response (status is equal to RepliesStatus.ok)
		// The service adapter (extended by this service class) provides method to create other types of
		// response
		XMLResponseREPLY respXml = createReplyResponse(RepliesStatus.ok);

		// Gets the data element of the response
		Element dataElem = respXml.getDataElement();

		// Simply sets the text content of the data element
		dataElem.setTextContent("Hi " + exPath);
				
		OWLModel model = getOWLModel();

		biblioExp(model, exPath);

		// Returns the response object
		return respXml;

		// A service method may have response type equal to void. In that case, the generated controller
		// is in charge of creating an empty reply response with status OK.

		// An exception thrown by a service method propagates through the generated controller, and then a
		// Controller Advice defined in the servlet context wraps it in the proper XML response.
	}
		
	/**
	 * Runs an importation, gathering the references of a file into the ontology
	 * 
	 * @param currentModel
	 * 
	 * @throws IOException
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 */
	public static void biblioImp(OWLModel currentModel, String fPath) throws IOException, UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException{
		filePath = fPath;
		
		biblioManager.fiToBi(filePath);
		biblioManager.biToOn(currentModel);
	}
	
	/**
	 * Runs the exporting process, generating a file from the ontology
	 * 
	 * @param currentModel
	 * 
	 * @throws IOException
	 * @throws UnsupportedQueryLanguageException
	 * @throws ModelAccessException
	 * @throws MalformedQueryException
	 * @throws QueryEvaluationException
	 */
	public static void biblioExp(OWLModel currentModel, String fPath) throws IOException, UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException, QueryEvaluationException{
		filePath = fPath+"\\Bibliography.bib";

		biblioManager.onToBi(currentModel);
		biblioManager.biToFi(filePath);
	}
}
