EXPORTED_SYMBOLS = [ "ExRequests"];

/**
 * @class
 */
ExRequests = function(){};

/**
 * @class
 */
ExRequests.BMan = function(){};

ExRequests.BMan.serviceName = "BMan";
ExRequests.BMan.importBibliographyRequest = "importBibliography";
ExRequests.BMan.exportBibliographyRequest = "exportBibliography";