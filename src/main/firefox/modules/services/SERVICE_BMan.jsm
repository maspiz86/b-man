Components.utils.import("resource://exModules/BMan.jsm");
Components.utils.import("resource://stmodules/Logger.jsm");
Components.utils.import("resource://stmodules/STHttpMgrFactory.jsm");
Components.utils.import("resource://stmodules/Context.jsm");

EXPORTED_SYMBOLS = ["ExRequests"];

var service = ExRequests.BMan;
var serviceName = service.serviceName;

var groupId = "it.uniroma2.art.semanticturkey";
var artifactId = "st_bman";

function importBibliography(imPath) {
	Logger.debug('[SERVICE_BMan.jsm] importBibliography');
	var p_imPath = "imPath=" + imPath;
	var currentSTHttpMgr = STHttpMgrFactory.getInstance(groupId, artifactId);
	return currentSTHttpMgr.GET(null, serviceName, service.importBibliographyRequest, this.context, p_imPath);
}

function exportBibliography(exPath) {
	Logger.debug('[SERVICE_BMan.jsm] importBibliography');
	var p_exPath = "exPath=" + exPath;
	var currentSTHttpMgr = STHttpMgrFactory.getInstance(groupId, artifactId);
	return currentSTHttpMgr.GET(null, serviceName, service.exportBibliographyRequest, this.context, p_exPath);
}

//this return an implementation for Project with a specified context
service.prototype.getAPI = function(specifiedContext){
	var newObj = new service();
	newObj.context = specifiedContext;
	return newObj;
}

service.prototype.importBibliography = importBibliography;
service.prototype.exportBibliography = exportBibliography;

service.prototype.context = new Context();  // set the default context
service.constructor = service;
service.__proto__ = service.prototype;