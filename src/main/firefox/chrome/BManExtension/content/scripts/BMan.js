if (typeof (art_stexp_ext) == "undefined") var art_stexp_ext = {};

Components.utils.import("resource://exServices/SERVICE_BMan.jsm", art_stexp_ext);

window.addEventListener("load",  function() {
	art_stexp_ext.init();
}, false);

art_stexp_ext.init = function() {
	document.getElementById("chooseImportPathB").addEventListener("command", art_stexp_ext.chooseFile, true);
	document.getElementById("chooseExportPathB").addEventListener("command", art_stexp_ext.saveFile, true);
	document.getElementById("importB").addEventListener("command", art_stexp_ext.importFile, true);
	document.getElementById("exportB").addEventListener("command", art_stexp_ext.exportOntology, true);
};

art_stexp_ext.chooseFile = function() {
	var nsIFilePicker = Components.interfaces.nsIFilePicker;
	var fp = Components.classes["@mozilla.org/filepicker;1"]
			.createInstance(nsIFilePicker);
	fp.init(window, "Select a File", nsIFilePicker.modeOpen);
	var res = fp.show();
	if (res == nsIFilePicker.returnOK) {
		var txbox = document.getElementById("importPathL");
		txbox.value = fp.file.path;
	}
};

art_stexp_ext.saveFile = function() {
	var nsIFilePicker = Components.interfaces.nsIFilePicker;
	var fp = Components.classes["@mozilla.org/filepicker;1"]
			.createInstance(nsIFilePicker);
	fp.init(window, "Select a Directory", nsIFilePicker.modeGetFolder);
	var res = fp.show();
	if (res == nsIFilePicker.returnOK) {
		var txbox = document.getElementById("exportPathL");
		txbox.value = fp.file.path;
	}
};

art_stexp_ext.importFile = function() {
	var imPath = document.getElementById("importPathL").value;
	if (imPath == "Importation Path") {
		alert("Please choose a path for the importation");
		return;
	}
	var xmlResp = art_stexp_ext.ExRequests.BMan.importBibliography(imPath);
	var importBibliographyResult = xmlResp.getElementsByTagName("data")[0].textContent;
	alert("Importation succeeded!");
};

art_stexp_ext.exportOntology = function() {
	var exPath = document.getElementById("exportPathL").value;
	if (exPath == "Exportation Path") {
		alert("Please choose a path for the exportation");
		return;
	}
	var xmlResp = art_stexp_ext.ExRequests.BMan.exportBibliography(exPath);
	var exportBibliographyResult = xmlResp.getElementsByTagName("data")[0].textContent;
	alert("Exportation succeeded!");
};