# BIBLIOMANAGER #

A Semantic Turkey (http://www.semanticturkey.uniroma2.it/) extension to manage bibliography files.

It allows both to import a bibliographic file into an ontology, both export an ontology into a bibliographic file.

## Requirements ##
* Semantic Turkey
* Mozilla Firefox